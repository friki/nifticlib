nifticlib (3.0.1-5) unstable; urgency=medium

  * Team upload.
  * Provide transitional package libnifti-dev to enable smooth migration.

 -- Andreas Tille <tille@debian.org>  Fri, 25 Dec 2020 14:32:51 +0100

nifticlib (3.0.1-4) unstable; urgency=medium

  * Remove remaining illegal Provides: libnifti-dev
  * Fix Provides for shared lib

 -- Andreas Tille <tille@debian.org>  Mon, 07 Dec 2020 16:09:58 +0100

nifticlib (3.0.1-3) unstable; urgency=medium

  * Team upload
  * libnifti2-dev: Depends: libniftiio-dev, libznz-dev
    (addresses reopened #968730)
  * Make sure devel packages are installing properly

 -- Andreas Tille <tille@debian.org>  Mon, 07 Dec 2020 15:10:57 +0100

nifticlib (3.0.1-2) unstable; urgency=medium

  * Team upload.
  * Source only upload

 -- Andreas Tille <tille@debian.org>  Fri, 04 Dec 2020 23:48:56 +0100

nifticlib (3.0.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
    Closes: #968730
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Make source package Section: science
  * Separate binary packages for libznz, libniftiio, libnifticdf

 -- Andreas Tille <tille@debian.org>  Tue, 01 Dec 2020 22:17:54 +0100

nifticlib (2.0.0+git186-g84740c2-1) unstable; urgency=medium

  * Team upload

  [ Yaroslav Halchenko ]
  * Imported upstream snapshot to see how well it builds etc
  * debian/patches
    - dropped all 00* patches which were cherry picked from upstream
    - disable build_setup patch -- should no longer be needed
  * debian/rules
    - updated list of cmake settings to not build cifti code for now
  * debian/libnifti-doc.examples -- install the entirety of the real_easy
  * debian/control
    - add graphviz to Build-Depends since used during documentation build

  [ Andreas Tille ]
  * Take over packaging into Debian Med team
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Remove trailing whitespace in debian/control (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Trim trailing whitespace.
  * Use versioned copyright format URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Build-Depends: python3
    Closes: #967180, #943196
  * Stick to old version 2 series to avoid bumping SOVERSION in binary
    package name since due to version bumps to libznz.so.3 and
    libnifti*.so.2.1.0
  * d/copyright: Fix syntax

 -- Andreas Tille <tille@debian.org>  Wed, 12 Aug 2020 10:38:13 +0200

nifticlib (2.0.0-3) unstable; urgency=medium

  * Team upload.
  * Point Vcs fields to Salsa
  * debhelper 11
  * Standards-Version: 4.2.1
  * hardening=+all

 -- Andreas Tille <tille@debian.org>  Thu, 27 Sep 2018 07:51:38 +0200

nifticlib (2.0.0-2) unstable; urgency=medium

  * The "I am not dead" release. General packaging updates, uncrufting,
    and making lintian v2.5.27 be satisfied.
    - Add all three patches commited to the upstream VCS since the 2.0.0
      release: e.g. improved memory management in case of IO errors.
  * Update uploader's email address.
  * Switch to debhelper compatibility level 9.
  * Replace custom makefile logic with dpkg-buildflags. This
    also enabled hardening features for compilation.
  * Bumped Standards-version to 3.9.5.
  * Removed obsolete DM-Upload tag.
  * Replace embedded jquery.js with a proper package dependency.
  * Rectify shell brace expansion in libnifti-dev.install.
  *

 -- Michael Hanke <mih@debian.org>  Mon, 06 Oct 2014 07:59:50 +0200

nifticlib (2.0.0-1) unstable; urgency=low

  * New release. Library changed API, hence bumped SO version to 2.0
    (Closes: #587912).
  * Switch to dpkg-source 3.0 (quilt) format. Split git feature branches into
    quilt patch series.
  * Drop versioned -dev package.
  * Change maintainer to NeuroDebian Team <team@neuro.debian.net>.
  * Switch from verbose debhelper rules to 'dh'.
  * Removed ECODE patch (merged with upstream).
  * Bumped Standards-version to 3.9.0. Now supports 'nocheck'.

 -- Michael Hanke <michael.hanke@gmail.com>  Tue, 20 Jul 2010 16:12:57 -0400

nifticlib (1.1.0-3) unstable; urgency=low

  * Disable building libfslio and remove the associated patches. The library
    is deprecated upstream. The affected Debian packages fsl and fslview
    will both build and use this library from fslview source code.
  * Included upstream-approved patch to add an ECODE for the PYPICKLE
    extension that is needed by PyNIfTI. Adjusted shlibdeps accordingly.
  * Added 'XS-DM-Upload-Allowed: yes' to debian/control.

 -- Michael Hanke <michael.hanke@gmail.com>  Thu, 19 Feb 2009 19:13:54 +0100

nifticlib (1.1.0-2) unstable; urgency=low

  * Added patch to libfslio from FSL 4.1: Removal of ANALYZE support.
    Additionally, FSL 4.1 changed the signature of FslSeekVolume(). This patch
    is not imported as it has to be coordinated with upstream (ABI change).
    But as fslio will be remove from the package with the next version, there
    is little need to do so.
  * Added shlibsdeps info with (<= 1.1.0-2) for the upcoming removal of fslio.
    This is only needed for fsl and fslview packages (the only ones using
    fslio), to prevent them from being broken, once a new stripped nifticlib
    package is uploaded.

 -- Michael Hanke <michael.hanke@gmail.com>  Fri, 24 Oct 2008 15:43:45 +0200

nifticlib (1.1.0-1) unstable; urgency=low

  * New upstream release. libniftiio1 contains some additional symbols --
    keeping SONAME and let dh_makeshlibs note dependency information (>= 1.1).
  * Do not install fsliolib scripts (im*,remove_ext,*.tcl) anymore. This part
    will most likely be removed from the package in future releases. So far,
    it has not been exposed in the default search path, but is only used
    internally by the fsl package. However, FSL itself comes with a more
    up-to-date copy of these scripts. The included ones are outdated and cannot
    be used as a dependency for FSL.
  * Convert debian/copyright into a machine-readable format and merge
    debian/README.legal into it.
  * Bump Standards-version to 3.8.0, no changes necessary.
  * Manpage generator for 'nifti_tool' now replaces all single-quotes with
    double-quotes to prevent confusing them with groff commands, if they are
    first character on a line (addressed lintian warning).

 -- Michael Hanke <michael.hanke@gmail.com>  Mon, 29 Sep 2008 12:38:07 +0200

nifticlib (1.0.0-1) unstable; urgency=low

  * New Upstream Version. New versioning scheme follows SO versions. All
    libraries in the package are no synchronized with respect to SO name
    and version.
  * Merged filename memory leak patch and ARM alignment patch (modified)
    with upstream.
  * Merged libfslio* packages with libniftiio*. As all library versions
    are now synchronized there is no need to keep libfslio separated.
    Especially as libfslio depends on libniftiio and is only a non-significant
    addition to the libniftiio package. Furthermore upstream is planning to
    merge libfslio with libniftiio in one of the next releases.
  * Renamed the only remaining library package pair to libnifti1. This is
    necessary to move all sub-libraries into one package without changing
    the SO name of libniftiio1 (with properly added conflicts to old
    libniftiio1 package).
  * Removed build-dep to d-shlibs as it cannot deal with new package name and
    install is now done manually.
  * Fixed some missing and some unnecessary symbols with correct linking.
  * Bumped Standards-version to 3.7.3, no changes necessary.

 -- Michael Hanke <michael.hanke@gmail.com>  Wed, 02 Jan 2008 20:46:21 +0100

nifticlib (0.6-3) unstable; urgency=low

  * Fix half-broken dependency of libfslio0-dev on no longer existing
    libniftiio0-dev.

 -- Michael Hanke <michael.hanke@gmail.com>  Wed, 28 Nov 2007 13:32:14 +0100

nifticlib (0.6-2) unstable; urgency=low

  * Compile 'unused' functions in nifticdf, because the prospective AFNI
    package depends on them. Set SO version of linifticdf to 0debian.1.0,
    because symbols were added.
  * Added new-style 'Hompage' and Vcs* fields to debian/control source stanza.
  * Included patch that fixes an illegal memory read that could occur when
    handling short filenames. Thanks to Yaroslav Halchenko.
  * Included patch that fixes a complicated issue that caused the library to
    fail on ARM. The reason was byte-swapping code that did not take the
    4-byte alignment on ARM into account. Please see
    http://netwinder.osuosl.org/users/b/brianbr/public_html/alignment.html
    for more info about this topic. The Debian maintainer is grateful to
    Dominique Belhachemi for starting the investigation and Yaroslav Halchenko
    for finally spotting and fixing this bug (Closes: #446893).

 -- Michael Hanke <michael.hanke@gmail.com>  Tue, 06 Nov 2007 20:01:05 +0100

nifticlib (0.6-1) unstable; urgency=low

  * New Upstream Version. This release introduces a new SO name for
    libniftiio. This was necessary to be able to handle NIfTI files larger
    than 2GB.
  * SO names of libznz and libnifticdf where set to a debian specific string
    ('0debian'). This was done to be able to keep them in one package with
    libniftiio1, without having to conflict with the previous libnifti0
    package. The Debian maintainer decided against the alternative solution of
    a package split (one package for each library), because upstream wants to
    adopt a VTK like versioning scheme in future release (syncronize library
    interface versions across all libs). The Debian package will then merge
    all libraries into one package, as each one alone isn't really useful.
  * Install the TCL wrapper for the image handling scripts in nifti-bin
    (fslio.tcl).
  * Updated package descriptions.
  * Include patch for fslio from FSL4. This patch introduces a number of new
    symbols, but doesn't change the API or ABI of the public interface.
    Therefore the SO name is kept constant.
  * Wrote individual manpages for the nifti-bin tools.
  * Disabled parts of the regression tests at build time, as they depend on
    datasets that are not part of the sources (have to be downloaded first).

 -- Michael Hanke <michael.hanke@gmail.com>  Wed,  5 Sep 2007 18:17:48 +0200

nifticlib (0.5-2) unstable; urgency=low

  * Add a patch to fslio to make it suitable for FSL 4.0. Bumped libfslio SO
    version due to some added symbols, but keeping SO name as only one
    internal function vanished (not previously exposed in the header file).
  * Removed leftover lintian override (Upstream does not include the CVS dirs
    anymore).
  * Added a little script to transform the help output of nifti_tool into a
    manpage (help2man does not work properly in this case). Added Python to
    the build-dependencies.
  * Removed custom SO-version setting from debian/rules as upstream sets
    proper SO-versions now.
  * Install fslio scripts into /usr/share/nifti to prevent naming conflicts
    with e.g. the 'im' package, but still let users and other packages make
    use of them.
  * Switch from ${Source-Version} to ${binary:Version} in debian/control.

 -- Michael Hanke <michael.hanke@gmail.com>  Mon, 23 Jul 2007 11:30:50 +0200

nifticlib (0.5-1) unstable; urgency=low

  * New upstream release.
  * Shared library patch merged with upstream.
  * The nifticdf library is now part of upstream sources. This make the AFNI
    nifticdf patch obsolete.
  * Currently no Debian-specific patches left, but keeping dpatch dependency.
  * Enable verbose compiler output by default.
  * Add .NOTPARALLEL flag to debian/rules (required by dpatch).
  * Listing which libraries are part of which package in the package
    description.
  * Use "-Wl,--as-needed" as linker flags to address problems reported by
    checklib.

 -- Michael Hanke <michael.hanke@gmail.com>  Tue,  5 Jun 2007 14:26:38 +0200

nifticlib (0.4+cvs20070424-1) unstable; urgency=low

  * New upstream CVS snapshot. No library interface changes, hence keeping
    current soversion.
  * Split nifti_stats sources into nifticdf library and nifti_stats
    application, to let the afni package use this library instead of using a
    copy of the sourcecode. libnifticdf is now part of the libniftiio*
    packages.
  * Perform tests at package build time.
  * Modified shared_lib support patch to match new upstream makefiles.

 -- Michael Hanke <michael.hanke@gmail.com>  Wed, 25 Apr 2007 20:17:04 +0200

nifticlib (0.4-1) unstable; urgency=low

  * New upstream release. This release mostly changes the copyright statements
    of fsliolib source code files to explicitely mention that they were
    previously part of FSL, but are now placed into the public domain.
  * Debian patches merged with upstream.

 -- Michael Hanke <michael.hanke@gmail.com>  Wed,  6 Sep 2006 21:47:35 +0200

nifticlib (0.3-2) unstable; urgency=low

  * Added appropriate 'Replaces' directives to debian/control to reflect the
    renaming of libnifti0 to libniftiio0 (and the separation of libfslio).

 -- Michael Hanke <michael.hanke@gmail.com>  Mon,  4 Sep 2006 09:59:32 +0200

nifticlib (0.3-1) unstable; urgency=low

  * Initial release. (Closes: #383349)
  * Added shared library support.
  * Added a lintian override concerning a single CVS directory in the upstream sources.
  * Added a lintian override that the library package is not named after the contained
    library. The NIFTI interfaces consists of three different libraries (compression,
    low-level and high-level) and it make no sense to me to split them into three
    binary packages.
  * Added manpage.

 -- Michael Hanke <michael.hanke@gmail.com>  Thu, 10 Aug 2006 22:14:20 +0200
